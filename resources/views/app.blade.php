<html>
<head>
    <title>Laravel 8 CRUD Tutorial Using Mysql Datatable</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha512-MoRNloxbStBcD8z3M/2BmnT+rg4IsMxPkXaGh2zD6LGNNFE80W3onsAhRcMAMrSoyWL9xD7Ert0men7vR8LUZg==" crossorigin="anonymous" />

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

</head>
<style type="text/css">
    .container{
        margin-top:10px;
    }
    h4{
        margin-bottom:20px;
    }
</style>
<body>
<div class="container">
    @yield('main-content')
</div>
</body>

</html>
