@extends('app')
@section('main-content')
    <div class="row">
        <div class="col-md-10 offset-md-2">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h4>Laravel 8 CRUD Tutorial Using Mysql Datatable</h4>
                </div>
                <a href="{{route('product.create')}}" class=" btn btn-outline-success mb-2">add new product</a>

                <div class="col-md-12">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                </div>
                <table class="table border">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Details</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($products as $product)
                        <tr>
                            <th scope="row">{{ ++$i}}</th>
                            <td>{{$product->title}}</td>
                            <td>{{\Str::limit($product->details, 50)}}</td>
                            <td>
                                <form action="{{ route('product.destroy',$product->id) }}" method="POST">

                                    <a class="btn btn-info" href="{{ route('product.show',$product->id) }}">Show</a>

                                    <a class="btn btn-primary" href="{{ route('product.edit',$product->id) }}">Edit</a>

                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>

                {!! $products->links() !!}

            </div>
        </div>
    </div>
    </div>

@endsection
